-- CreateTable
CREATE TABLE "Message" (
"id" SERIAL,
    "content" TEXT,
    "authorId" INTEGER NOT NULL,
    "roomId" INTEGER NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "User" (
"id" SERIAL,
    "name" TEXT NOT NULL,
    "password" TEXT,
    "roomId" INTEGER,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Room" (
"id" SERIAL,
    "name" TEXT NOT NULL,

    PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "User.name_unique" ON "User"("name");

-- CreateIndex
CREATE UNIQUE INDEX "Room.name_unique" ON "Room"("name");

-- AddForeignKey
ALTER TABLE "Message" ADD FOREIGN KEY("authorId")REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Message" ADD FOREIGN KEY("roomId")REFERENCES "Room"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "User" ADD FOREIGN KEY("roomId")REFERENCES "Room"("id") ON DELETE SET NULL ON UPDATE CASCADE;
