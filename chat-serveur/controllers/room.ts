import { Request, Response, NextFunction } from 'express';
import ConnectionDispatcher from "../src/connection-dispatcher";
import roomRepo from "../src/repository/room-repo";

class RoomController {

    getSocket(req: Request, res: Response, next: NextFunction) {
        
    }

    postFindRoom(req: Request, res: Response, next: NextFunction) {
        const user = res.locals.user;
        const room = res.locals.room;

        return res.redirect(`//localhost:3000/room/${room.name}`);
    }

    async getRoomMessages(req: Request, res: Response, next: NextFunction) {
        console.log(roomRepo.findRoomMessages(res.locals.room.name));
    }
}

export default new RoomController();