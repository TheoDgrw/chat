import { Request, Response, NextFunction } from 'express';
import roomRepo from '../src/repository/room-repo';
import userRepo from "../src/repository/user-repo";
import prisma from "../utils/prisma-client";


class Authentication {

    async authenticateUser(req: Request, res: Response, next: NextFunction) {
        const username = req.body.username;
        let roomName = req.body.roomname;

        res.locals.user = await userRepo.authentication(username);
        res.locals.room = await prisma.room.findUnique({ where: { name: roomName } });

        if (!res.locals.user) {
            res.locals.user = await userRepo.create(username);
        }
        if (!res.locals.room) {
            res.locals.room = roomRepo.create(roomName);
        }

        next();
    }
}

export default new Authentication();