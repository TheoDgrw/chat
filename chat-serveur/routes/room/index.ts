import express = require('express');

import roomController from '../../controllers/room';
import authenticationController from '../../controllers/authentication';

const router = express.Router();

router.post('/find-room', authenticationController.authenticateUser, roomController.postFindRoom);
router.get('/get-room-messages', authenticationController.authenticateUser, roomController.getRoomMessages);

export default router;
