import express = require('express');

import roomRoutes from './room';

const router = express.Router();

router.use('/room', roomRoutes);

export default router;
