import express = require('express');
import { Socket } from 'socket.io';
const ioc = require('socket.io-client');

import routes from './routes';
import SocketRemote from './src/socket/commands/socket-remote';
import RoomManager from './src/socket/room-manager/Room';

const app = express();

const http = require('http').createServer(app);

export const io = require('socket.io')(http, {
    cors: 'http://localhost:3000'
});

io.on('connection', (socket: Socket) => {
    console.log(socket);
    new RoomManager(socket);
    // console.log('test');
    // socket.on('chat message', (message: string) => {
    //     console.log(message);
    // });
});

app.use(routes);

http.listen(4000, () => {
    console.log('server running on port 4000');
});