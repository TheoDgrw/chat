import Room from '../room/Room';
import { Socket } from "socket.io";


export default abstract class Command {

    protected socket: Socket;

    constructor(socket: Socket) {
        this.socket = socket;
    }
    
    abstract execute(): void;
}