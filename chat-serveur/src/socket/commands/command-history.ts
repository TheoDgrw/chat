import Command from "./command";
import CommandSendMessage from "./command-send-message";
import UserConnect from "./command-user-connect";

export default class CommandHistory {
    public commands: Command[] = [];

    push(command: Command) {
        this.commands.push(command);
    }

    getMessages(): Command[] {

        return this.commands.filter((command: Command) => {
            if (command instanceof CommandSendMessage) {

                return command;
            }
        });
    }

    getUsersConnections(): Command[] {

        return this.commands.filter((command: Command) => {
            if (command instanceof UserConnect) {

                return this.commands;
            }
        })
    }
}