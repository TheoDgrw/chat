import { io } from "../../../app";
import { Socket as ISocket } from 'socket.io';
import ICommand from "../interfaces/socket/icommand";
import SendMessage from "./command-send-message";
import Command from "./command";
import UserConnect from "./command-user-connect";


export default class SocketRemote {
    protected commands: Command[] = [];
    protected socket: ISocket;

    constructor(socket: ISocket) {
        this.socket = socket;
        this.addCommand(new SendMessage(socket));
        this.addCommand(new UserConnect(socket));
    }

    addCommand(command: Command) {
        this.commands.push(command);
    }

    // TODO: déplacer dans Room
    // connection() {
    //     this.io.on('connection', () => {
    //         new UserConnect(this.socket)
    //     });
    // }

    // TODO: supprimer ?
    execute(commandName: string) {
        switch (commandName) {
            case 'send-message':
                this.commands[1].execute();
            case 'user-connect':
                this.commands[2].execute();
            default:
                console.log('COMMAND STRING DOES NOT EXIST');
        }
    }
}