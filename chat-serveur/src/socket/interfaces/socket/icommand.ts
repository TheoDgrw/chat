import { Socket as ISocket } from 'socket.io';

export default interface ICommand {

    execute(socket: ISocket): void;
}