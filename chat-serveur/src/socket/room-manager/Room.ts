import { User, Message } from "@prisma/client";
import Command from "../commands/command";
import CommandSendMessage from "../commands/command-send-message";
import UserConnect from "../commands/command-user-connect";
import Remote from "../commands/socket-remote";
import messageRepo from "../../repository/message-repo";

import prisma from "../../../utils/prisma-client";
import { Socket } from "socket.io";

type queryObj = {
    room: string,
    user: string
}

// TODO: remplacer history par messages
export default class RoomManager {
    protected remote: Remote;
    protected socket: Socket;

    // TODO: ajouter arguments de Remote
    constructor(socket: Socket) {
        this.socket = socket;
        this.remote = new Remote(socket);
        this.remote.execute('user-connect');
        const roomname = (<queryObj>socket.handshake.query).room;
        socket.join(roomname);
        socket.on('chat message', (mess: string) => this.chatListener(mess));
    }

    chatListener(message: string) {
        const query = (<queryObj>this.socket.handshake.query)
        messageRepo.create(message, query.user, query.room)
        this.socket.to(query.room).emit(message);
    }
}