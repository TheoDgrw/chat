import prisma from "../../utils/prisma-client";
import { Message, Room } from "@prisma/client"


class MessageRepository {

    async create(content: string, username: string, roomname: string) {
        const user = await prisma.user.findUnique({ where: { name: username }});

        return await prisma.message.create({
            data: {
                content: content,
                created_at: new Date(),
                author: {
                    connect: {
                        name: user?.name
                    }
                },
                room: {
                    connect: {
                        name: roomname
                    }
                }
            }
        });
    }
}

export default new MessageRepository();