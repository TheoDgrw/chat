import prisma from "../../utils/prisma-client";
import { Message, Room, User } from "@prisma/client";


class UserRepository {

    async create(username: string): Promise<User> {

        return await prisma.user.create({
            data: {
                name: username
            }
        });
    }

    async authentication(username: string): Promise<User | null> {

        return await prisma.user.findUnique({
            where: { name: username }
        });
    }
}

export default new UserRepository();