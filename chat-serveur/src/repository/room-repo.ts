import prisma from "../../utils/prisma-client";
import { Message, Room, User } from "@prisma/client";


class RoomRepository {

    async create(name: string): Promise<Room> {
        return await prisma.room.create({
            data: {
                name: name
            }
        });
    }

    async findRoomMessages(name: string): Promise<Message[]> {
        return await prisma.room.findUnique({ where: { name: name }}).messages();
    }

    async findRoomUsers(name: string): Promise<User[]> {
        return await prisma.room.findUnique({ where: { name: name }}).users();
    }

    async isRoom(name: string): Promise<Room | false> {
        const room = await prisma.room.findUnique({ where: { name: name }});

        return room ? room : false;
    }
}

export default new RoomRepository();