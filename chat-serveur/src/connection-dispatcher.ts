import { Socket } from "socket.io";
import roomRepo from "./repository/room-repo";


export default class ConnectionDispatcher {
    userName: string;
    roomName: string;

    constructor(userName: string, roomName: string) {
        this.userName = userName;
        this.roomName = roomName;
    }

    public async checkRoom() {
        const room = await roomRepo.isRoom(this.roomName);

        if (room) {

            return room;
        }
        
        return await roomRepo.create(this.roomName);
    }
}