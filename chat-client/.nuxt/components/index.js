export { default as Chat } from '../../components/chat.vue'
export { default as Header } from '../../components/header.vue'
export { default as Message } from '../../components/message.vue'
export { default as Users } from '../../components/users.vue'

export const LazyChat = import('../../components/chat.vue' /* webpackChunkName: "components/chat" */).then(c => c.default || c)
export const LazyHeader = import('../../components/header.vue' /* webpackChunkName: "components/header" */).then(c => c.default || c)
export const LazyMessage = import('../../components/message.vue' /* webpackChunkName: "components/message" */).then(c => c.default || c)
export const LazyUsers = import('../../components/users.vue' /* webpackChunkName: "components/users" */).then(c => c.default || c)
